# TASK MANAGER

## DEVELOPER INFO

**NAME:** Bondarenko Maksim

**EMAIL:** iwealth@yandex.ru

## SYSTEM INFO

**OS**: Linux Mint 21.1

**JDK**: Java 1.8

**RAM**: 16GB

**CPU**: i7

## BUILD PROJECT

````
mvn clean install
````

## RUN PROJECT

````
cd ./target
java -jar ./task-manager.jar
````
